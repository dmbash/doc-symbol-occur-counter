const fReader = require('any-text');

const getContent = (filename) => fReader.getText(filename)
  .then((data) => data);

function getEmptyMap() {
  const reducer = (map, v) => {
    map[v] = 0;
    return map;
  };
  return {
    storage: new Array(64)
      .fill(1)
      .map((_, i) => String.fromCharCode('А'.charCodeAt(0) + i))
      .reduce(reducer, {}),
    increment(symbol) {
      if (this.storage.hasOwnProperty(symbol)) {
        this.storage[symbol] += 1;
      }
    },
  };
}

const getOccuriencesMap = (content) => {
  const map = getEmptyMap();

  for (let i = 0, len = content.length; i < len; i += 1) {
    map.increment(content[i]);
  }

  return map;
};

async function main() {
  const text = await getContent('file.doc');

  const occuriencesMap = getOccuriencesMap(text);

  console.log(occuriencesMap.storage);
}

main();
